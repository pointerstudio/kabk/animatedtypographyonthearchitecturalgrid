# Animated Typography on the Architectural Grid

There are of course endless possibilities for creating animated typography, but here are a few to get you started. Because we only have limited time in the workshop list to choose one or two that best suits your building style and research it.

First style frames

## Get started with Cavalry

## Importing from illustrator
If you paste a file from illustrator, you can Separate to editable or Merge to editable to edit the shape
1. Pick the shape tap from the top menu and pick
2. Separate to editable / Merge to editable

## 1. Morphing by animate path
- Change smoothly from one shape to another.
- Draw a shape in cavalry (or paste from illustrator)
- Double click a shape in the scene window
- Make a keyframe in the Attribute editor
- Make a second Keyframe 
- Adjust the keyframes.
(It can be useful to make an extra layer to use as a guide)

## 2. Duplicator
- Make a shape
- Create a duplicator in scene presets
- Adjust settings

## 3a. Image sampler
- Add image sampler to scene window
- Double click the image sampler
- Drag a movie or video in “image Asset”
- Drag the dot next to the Image sampler onto the Duplicator
- Pick a value you want it to control
- Turn image sampler invisible
- https://www.youtube.com/watch?v=2fuCsw1XT7k

## 3b. Image sampler with number range
- Drag the dot next to the “Image sampler” to “number range”
- Select “value”
- Drag the dot next to the “number range” to “duplicator” and select for instance shape scale

## 4. Animated modules (modular typeface)
- paste a shape from illustrator
- Separate to editable
- Now you can animate all the separate elements
(Tip if your typeface is consisting of repeating elements you can also reuse the animations)

## 5. Animated Mask
- Open the mask tab in the Attribute editor 
- Drag a shape to the clipping mask area.
(Tip you can also animate a mask)

Here you can find more info on cavalry:
https://docs.cavalry.scenegroup.co

Here you can find more on Easing
https://easings.net

# more on projection mapping
https://gitlab.com/pointerstudio/kabk/projectprojector
